import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() { 
      GraphTask.main (null);
      assertTrue ("There are no tests", true);
   }

   @Test (timeout = 1000)
   public void test_two_vertex_graph_centre_should_have_the_same_vertices(){
      GraphTask gt = new GraphTask();
      GraphTask.Graph g = gt.new Graph("G");
      GraphTask.Vertex v1 = gt.new Vertex("V1");
      GraphTask.Vertex v2 = gt.new Vertex("V2");
      List<GraphTask.Vertex> vertices = new ArrayList<>();
      vertices.add(v1);
      vertices.add(v2);

      GraphTask.Arc v1v2 = gt.new Arc("av1_v2");
      GraphTask.Arc v2v1 = gt.new Arc("av2_v1");

      g.setFirst(v1);
      v1.setNext(v2);

      v1.setFirst(v1v2);
      v1v2.setTarget(v2);
      v1v2.setNext(null);
      v2.setFirst(v2v1);
      v2v1.setTarget(v1);
      List<GraphTask.Vertex> result = g.getCentre();
      for (int i = 0; i < vertices.size(); i++) {
         assertEquals(vertices.get(i), result.get(i));
      }
   }
   @Test (expected=RuntimeException.class)
   public void test_graph_without_vertices_should_throw_exception_on_centre_calculation(){
      GraphTask gt = new GraphTask();
      GraphTask.Graph g = gt.new Graph("G");
      g.getCentre();
   }

   @Test (timeout = 1000)
   public void test_graph_centre_should_be_three_vertices(){
      GraphTask gt = new GraphTask();
      GraphTask.Graph g = gt.new Graph("G");
      GraphTask.Vertex v1 = gt.new Vertex("V1");
      GraphTask.Vertex v2 = gt.new Vertex("V2");
      GraphTask.Vertex v3 = gt.new Vertex("V3");
      GraphTask.Vertex v4 = gt.new Vertex("V4");
      GraphTask.Vertex v5 = gt.new Vertex("V5");
      GraphTask.Vertex v6 = gt.new Vertex("V6");
      GraphTask.Vertex v7 = gt.new Vertex("V7");
      GraphTask.Vertex v8 = gt.new Vertex("V8");
      GraphTask.Vertex v9 = gt.new Vertex("V9");
      List<GraphTask.Vertex> expected = new ArrayList<>();
      expected.add(v3);
      expected.add(v4);
      expected.add(v5);

      GraphTask.Arc v1v2 = gt.new Arc("av1_v2");
      GraphTask.Arc v2v1 = gt.new Arc("av2_v1");
      GraphTask.Arc v2v3 = gt.new Arc("av2_v3");
      GraphTask.Arc v3v2 = gt.new Arc("av3_v2");
      GraphTask.Arc v3v5 = gt.new Arc("av3_v5");
      GraphTask.Arc v5v8 = gt.new Arc("av5_v8");
      GraphTask.Arc v8v5 = gt.new Arc("av8_v5");
      GraphTask.Arc v5v3 = gt.new Arc("av5_v3");
      GraphTask.Arc v3v4 = gt.new Arc("av3_v4");
      GraphTask.Arc v4v3 = gt.new Arc("av4_v3");
      GraphTask.Arc v4v6 = gt.new Arc("av4_v6");
      GraphTask.Arc v4v7 = gt.new Arc("av4_v7");
      GraphTask.Arc v5v7 = gt.new Arc("av5_v7");
      GraphTask.Arc v9v7 = gt.new Arc("av9_v7");
      GraphTask.Arc v7v4 = gt.new Arc("av7_v4");
      GraphTask.Arc v7v5 = gt.new Arc("av7_v5");
      GraphTask.Arc v7v9 = gt.new Arc("av7_v9");
      GraphTask.Arc v6v4 = gt.new Arc("av6_v4");

      g.setFirst(v1);
      v1.setNext(v2);
      v2.setNext(v3);
      v3.setNext(v4);
      v4.setNext(v5);
      v5.setNext(v6);
      v6.setNext(v7);
      v7.setNext(v8);
      v8.setNext(v9);

      v1.setFirst(v1v2);
      v1v2.setTarget(v2);

      v2.setFirst(v2v1);
      v2v1.setTarget(v1);
      v2v1.setNext(v2v3);
      v2v3.setTarget(v3);

      v3.setFirst(v3v4);
      v3v4.setTarget(v4);
      v3v4.setNext(v3v2);
      v3v2.setTarget(v2);
      v3v2.setNext(v3v5);
      v3v5.setTarget(v5);

      v4.setFirst(v4v3);
      v4v3.setTarget(v3);
      v4v3.setNext(v4v6);
      v4v6.setTarget(v6);
      v4v6.setNext(v4v7);
      v4v7.setTarget(v7);

      v5.setFirst(v5v3);
      v5v3.setTarget(v3);
      v5v3.setNext(v5v8);
      v5v8.setTarget(v8);
      v5v8.setNext(v5v7);
      v5v7.setTarget(v7);

      v6.setFirst(v6v4);
      v6v4.setTarget(v4);

      v7.setFirst(v7v4);
      v7v4.setTarget(v4);
      v7v4.setNext(v7v5);
      v7v5.setTarget(v5);
      v7v5.setNext(v7v9);
      v7v9.setTarget(v9);

      v8.setFirst(v8v5);
      v8v5.setTarget(v5);

      v9.setFirst(v9v7);
      v9v7.setTarget(v7);

      List<GraphTask.Vertex> result = g.getCentre();
      for (int i = 0; i < expected.size(); i++) {
         assertEquals(expected.get(i), result.get(i));
      }

   }

   @Test (timeout = 1000)
   public void test_square_graph_should_have_all_vertices_in_centre() {
      GraphTask gt = new GraphTask();
      GraphTask.Graph g = gt.new Graph("G");
      GraphTask.Vertex v1 = gt.new Vertex("V1");
      GraphTask.Vertex v2 = gt.new Vertex("V2");
      GraphTask.Vertex v3 = gt.new Vertex("V3");
      GraphTask.Vertex v4 = gt.new Vertex("V4");
      List<GraphTask.Vertex> vertices = new ArrayList<>();
      vertices.add(v1);
      vertices.add(v2);
      vertices.add(v3);
      vertices.add(v4);

      GraphTask.Arc v1v2 = gt.new Arc("av1_v2");
      GraphTask.Arc v1v4 = gt.new Arc("av1_v4");

      GraphTask.Arc v2v1 = gt.new Arc("av2_v1");
      GraphTask.Arc v2v3 = gt.new Arc("av2_v3");

      GraphTask.Arc v3v2 = gt.new Arc("av3_v2");
      GraphTask.Arc v3v4 = gt.new Arc("av3_v4");

      GraphTask.Arc v4v1= gt.new Arc("av4_v1");
      GraphTask.Arc v4v3= gt.new Arc("av4_v3");

      g.setFirst(v1);
      v1.setNext(v2);
      v2.setNext(v3);
      v3.setNext(v4);

      v1.setFirst(v1v2);
      v1v2.setTarget(v2);
      v1v2.setNext(v1v4);
      v1v4.setTarget(v4);

      v2.setFirst(v2v1);
      v2v1.setTarget(v1);
      v2v1.setNext(v2v3);
      v2v3.setTarget(v3);

      v3.setFirst(v3v2);
      v3v2.setTarget(v2);
      v3v2.setNext(v3v4);
      v3v4.setTarget(v4);

      v4.setFirst(v4v1);
      v4v1.setTarget(v1);
      v4v1.setNext(v4v3);
      v4v3.setTarget(v3);
      List<GraphTask.Vertex> result = g.getCentre();
      for (int i = 0; i < vertices.size(); i++) {
         assertEquals(vertices.get(i), result.get(i));
      }
   }

}

