import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @since 1.8
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();

   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("Square");
      Vertex v1 = new Vertex("V1");
      Vertex v2 = new Vertex("V2");
      Vertex v3 = new Vertex("V3");
      Vertex v4 = new Vertex("V4");

      List<Vertex> vertices = new ArrayList<>();
      vertices.add(v1);
      vertices.add(v2);
      vertices.add(v3);
      vertices.add(v4);

      Arc v1v2 = new Arc("av1_v2");
      Arc v1v4 = new Arc("av1_v4");

      Arc v2v1 = new Arc("av2_v1");
      Arc v2v3 = new Arc("av2_v3");

      Arc v3v2 = new Arc("av3_v2");
      Arc v3v4 = new Arc("av3_v4");

      Arc v4v1= new Arc("av4_v1");
      Arc v4v3= new Arc("av4_v3");


      g.first = v1;
      v1.next =v2;
      v2.next = v3;
      v3.next = v4;

      v1.first = v1v2;
      v1v2.target = v2;
      v1v2.next = v1v4;
      v1v4.target = v4;

      v2.first = v2v1;
      v2v1.target =v1;
      v2v1.next = v2v3;
      v2v3.target = v3;

      v3.first = v3v2;
      v3v2.target = v2;
      v3v2.next = v3v4;
      v3v4.target = v4;

      v4.first = v4v1;
      v4v1.target = v1;
      v4v1.next = v4v3;
      v4v3.target = v3;

      System.out.println(g);

      if (!g.getCentre().equals(vertices)) throw new AssertionError();
      System.out.println("Centre of square graph " + g.getCentre());

   }

   // TODO!!! add javadoc relevant to your problem
   /**
    * Don't have problem with vertex.
    * The problem I'm solving is about the whole graph.
    * The essence of this exercise is to create method which
    * will return centre of the given graph.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public void setNext(Vertex next) {
         this.next = next;
      }

      public void setFirst(Arc first) {
         this.first = first;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public void setTarget(Vertex target) {
         this.target = target;
      }

      public void setNext(Arc next) {
         this.next = next;
      }
   } 


   /** This header represents a graph.
    */ 
   class Graph {

      private String id;

      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      public void setFirst(Vertex first) {
         this.first = first;
      }

      /**
       * For debugging, prints to console the matrix
       * @param matrix 2D matrix of int
       */
      private void printMatrix (int[][] matrix){
         for (int[] row: matrix) {
            StringBuilder sb = new StringBuilder();
            for ( int el : row) {
               sb.append(el);
               sb.append(" ");
            }
            System.out.println(sb);
         }
      }

      /**
       *
       * @return int array of the graph vertices distances
       */
      private int[][] getDistanceMatrix(){
         int[][] adjMatrix = this.createAdjMatrix();
         //Idea from https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html Kauguste arvutamine
         int len = adjMatrix.length;
         int[][] result = new int[len][len];
         if (len < 1) return result;
         int INFINITY = 2 * len + 1;
         for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
               if (adjMatrix[i][j] == 0){
                  result[i][j] = INFINITY;
               } else {
                  result[i][j] = 1;
               }
            }
         }
         for (int i = 0; i < len; i++) {
            result[i][i] = 0;
         }
         return result;
      }

      /**
       * Uses Floyd-Warshall algorithm to generate matrix
       * of the shortest paths between all vertices
       * @return multidimensional int array
       */
      private int[][] getShortestPaths(){
         int[][] result = this.getDistanceMatrix();
         int len = result.length;
         if (len < 1) return result;
         for (int k = 0; k < len; k++) {
            for (int i = 0; i < len; i++) {
               for (int j = 0; j < len; j++) {
                  if (result[i][j] > result[i][k] + result[k][j]){
                     result[i][j] = result[i][k] + result[k][j];
                  }
               }
            }
         }
         return result;
      }

      /**
       *
       * @return list of Vertices which are in the centre of the graph
       */
      public List<Vertex> getCentre(){
         List<Vertex> result = new ArrayList<>();
         List<Vertex> vertices = new ArrayList<>();
         int[][] shortestPaths = this.getShortestPaths();
         int minPath = 0;

         try {
            minPath = Arrays.stream(shortestPaths[0]).max().getAsInt();
         } catch (Exception e){
            throw new RuntimeException("Can't calculate centre for nothing");
         }

         for (int[] row: shortestPaths) {
            int rowMax = Arrays.stream(row).max().getAsInt();
            if (rowMax < minPath){
               minPath = rowMax;
            }
         }

         for (Vertex v = this.first; v != null; v = v.next){
            vertices.add(v);
         }

         for (int i = 0; i < shortestPaths.length; i++) {
            if (Arrays.stream(shortestPaths[i]).max().getAsInt() == minPath){
               result.add(vertices.get(i));
            }
         }

         return result;
      }
   }
} 

